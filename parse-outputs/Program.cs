﻿using System;
using System.IO;
using System.Reflection;


//  I might need to revisit the parsing code as I look for the IP addresses at 35 chars in. That might need to be more flexible.
//  i.e. some kind of instr


 //{
//    "PuppetMasterIP1":{
//      "Value":{"Ref":"MasterEIP1"},
//      "Description":"Public IP of Puppet master"
//    },


//    "PuppetAgentIP1":{
//      "Value":{"Ref":"AgentEIP1"},
//      "Description":"Public IP of Puppet agent"
//    }

//,
       

namespace parse_outputs
{
        //-i stack-outputs.txt -d D:\(Puppet)\_Build\
        class Program
        {
            static void Main(string[] args)
            {

                //Constants for fragment source files
                const string _resource_output = "parse_outputs.fragment-output.txt";

                const string _inputfile_default = "stack-outputs.txt";
                const string _outputdirectory_default = "";     //hoping this will be current directory. might need to call a function to get it

                //string variables 
                string fragment_output = "";

                string inputfilename = _inputfile_default;
                string outputdirectory = _outputdirectory_default;

                
                string finalstring = "";


                InputArguments arguments = new InputArguments(args);


                if (!arguments.Contains("-i"))
                    Console.WriteLine(string.Format("so is not set. Will default to {0}", _inputfile_default));
                else
                    inputfilename = arguments["-i"];


                if (!arguments.Contains("-d"))
                    Console.WriteLine("d is not set. Will default to local directory  {0}", _outputdirectory_default);
                else
                    outputdirectory = arguments["-d"];


                var assembly = Assembly.GetExecutingAssembly();


                //Read in output fragment
                using (Stream stream = assembly.GetManifestResourceStream(_resource_output))
                using (StreamReader reader = new StreamReader(stream))
                {
                    fragment_output = reader.ReadToEnd();
                }






                string[] lines = System.IO.File.ReadAllLines(inputfilename);
                string line = "";
                int studentnumber = 0;

                //I am stepping through the array rather than using find in a single string and use instr
                //perhaps I can try that with version 2.0
                for (int l = 0; l < lines.Length; l++)
                {
                    line = lines[l];

                    //Look for first OutputKey in pair
                    //"OutputKey": "PuppetMasterIP1", 
                    if (line.IndexOf("PuppetMasterIP") != -1)
                    {
                        //Found a match
                        studentnumber++;

                        //Get next one
                        //"OutputValue": "54.154.58.94"
                        line = lines[++l];

                     
                        string tempstring = line.Substring(35); //35 should be just before the quote preceeding the IP address

                        //might need PathTooLongException remove quptes
                        finalstring = fragment_output.Replace("Master-IP", tempstring);


                    }

                    //Look for second OutputKey in pair
                    //"OutputKey": "PublicIP1", 
                    if (line.IndexOf("PuppetAgentIP") != -1)
                    {
                        //Found a match                       

                        //Get next one
                        //"OutputValue": "54.77.49.18"
                        line = lines[++l];

                        string tempstring = line.Substring(35); //35 should be just before the quote preceeding the IP address

                        //might need PathTooLongException remove quptes
                        finalstring = finalstring.Replace("Agent-IP", tempstring);


                        //write file student + number .txt to output directory
                        System.IO.File.WriteAllText(outputdirectory + "student" + studentnumber.ToString() + ".txt", finalstring);

                    }


                }


            }
        }

}
